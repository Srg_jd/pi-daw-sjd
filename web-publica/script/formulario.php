﻿<?php

    $error = '';

    $nombre = $_POST["nombre"];
    $nombreLimpio = trim(filter_var($nombre, FILTER_SANITIZE_STRING));

    //VALIDANDO NOMBRE
    if(empty($nombre)){
        $error = 'Ingresa un nombre </br>';
    }
    else if ($nombreLimpio==''){
            $error .= 'El nombre no puede estar vacío.</br>';
    }
    else if (is_numeric($nombre)){
        $error = 'El nombre no puede incluir números.</br>';
    }

    //VALIDANDO TELEFONO
    if(empty($_POST["tel"])){
        $error .= 'Ingresa un teléfono</br>';

    }else{
        $tel = $_POST["tel"];

        if(!filter_var($tel,FILTER_SANITIZE_NUMBER_INT)){
            $error .= 'Ingresa un teléfono válido</br>';
        }else{
            $tel = filter_var($tel,FILTER_SANITIZE_EMAIL);
        }
    }

    //VALIDANDO E-MAIL
    if(empty($_POST["email"])){
        $error .= 'Ingresa un E-mail</br>';

    }else{
        $email = $_POST["email"];

        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            $error .= 'Ingresa un E-mail válido</br>';
        }else{
            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
        }
    }
    //VALIDANDO MENSAJE
    if(empty($_POST["mensaje"])){
        $error .= 'Ingresa un mensaje </br>';
    }else{
        $mensaje = $_POST["mensaje"];
        $mensaje = filter_var($mensaje, FILTER_SANITIZE_STRING);
        $mensaje = trim($mensaje);
        if($mensaje==''){
            $error .= 'Mensaje está vacio</br>';
        }
    }

    //CUERPO DEL MENSAJE
    $cuerpo .= "Nombre: ";
    $cuerpo .= $nombre;
    $cuerpo .= "\n";

    $cuerpo .= "Teléfono: ";
    $cuerpo .= $tel;
    $cuerpo .= "\n";

    $cuerpo .= "Email: ";
    $cuerpo .= $email;
    $cuerpo .= "\n";

    $cuerpo .= "Mensaje: ";
    $cuerpo .= $mensaje;
    $cuerpo .= "\n";

    //DIRECCIÓN
    $enviarA = 'sjuandiez@gmail.com'; //REEMPLAZAR CON TU CORREO ELECTRÓNICO
    $asunto = 'Nuevo mensaje de mi sitio web';

    //ENVIAR CORREO
    if($error == ''){
        $success = mail($enviarA,$asunto,$cuerpo,'de: '.$email);
        echo 'exito';
    }else{
        echo $error;
    }

?>
