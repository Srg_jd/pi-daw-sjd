var height = $(window).height();
    var width = $(window).width();
    var heightFormulario = $('#secForm').height();

    smoothScroll.init({
      selector: '[data-scroll]',
      selectorHeader: null,
      speed: 2000,
      easing: 'easeInOutCubic',
      offset: 100,
      callback: function(anchor, toggle) {}
    });

    $('.navbar-collapse a').click(function() {
      $(".navbar-collapse").collapse('hide');
    });


    $(document).ready(function() {
      if (width > 500) {
        $('.alturaSection').height(height);
        $('#secMaps').height(heightFormulario);

      }
    });