//console.log("funcionando");
$("#formulario").submit(function(event){
    event.preventDefault(); //almacena los datos sin refrescar el sitio web.
    enviar();
});
function enviar(){
    //console.log("ejecutado");
    var datos = $("#formulario").serialize(); //toma los datos "name" y los lleva a un arreglo.
    $.ajax({
        type: "post",
        url:"script/formulario.php",
        data: datos,
        success: function(texto){
            if(texto=="exito"){
                correcto();
            }else{
                phperror(texto);
            }
        }
    })
}
function correcto(){
    $("#mensajeExito").removeClass("d-none");
    $("#mensajeError").addClass("d-none");
//    $(".form-control").value = "";
  //  document.getElementById("nombre").value = "";
  $("#formulario")[0].reset();
}
function phperror(texto){
    $("#mensajeExito").addClass("d-none");
    $("#mensajeError").removeClass("d-none");
    $("#mensajeError").html(texto);
}