INSERT INTO regiones (id, nombre) VALUES (1, 'Andalucía');
INSERT INTO regiones (id, nombre) VALUES (2, 'Aragón');
INSERT INTO regiones (id, nombre) VALUES (3, 'Principado de Asturias');
INSERT INTO regiones (id, nombre) VALUES (4, 'Illes Balears');
INSERT INTO regiones (id, nombre) VALUES (5, 'Canarias');
INSERT INTO regiones (id, nombre) VALUES (6, 'Cantabria');
INSERT INTO regiones (id, nombre) VALUES (7, 'Castilla y León');
INSERT INTO regiones (id, nombre) VALUES (8, 'Castilla-La Mancha');
INSERT INTO regiones (id, nombre) VALUES (9, 'Cataluña');
INSERT INTO regiones (id, nombre) VALUES (10, 'Comunitat Valenciana');
INSERT INTO regiones (id, nombre) VALUES (11, 'Extremadura');
INSERT INTO regiones (id, nombre) VALUES (12, 'Galicia');
INSERT INTO regiones (id, nombre) VALUES (13, 'Comunidad de Madrid');
INSERT INTO regiones (id, nombre) VALUES (14, 'Región de Murcia');
INSERT INTO regiones (id, nombre) VALUES (15, 'Comunidad Foral de Navarra');
INSERT INTO regiones (id, nombre) VALUES (16, 'País Vasco');
INSERT INTO regiones (id, nombre) VALUES (17, 'La Rioja');
INSERT INTO regiones (id, nombre) VALUES (18, 'Ciudad Autónoma de Ceuta');
INSERT INTO regiones (id, nombre) VALUES (19, 'Ciudad Autónoma de Melilla');

INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(1, 'Sergio', 'Juan', 'sjuan@gmail.com', '2018-01-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(2, 'Paco', 'Perez', 'pperez@gmail.com', '2018-01-02');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Nerea', 'Rodriguez', 'nrod@gmail.com', '2018-01-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Benita', 'Diez', 'bdiez@gmail.com', '2018-01-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(4, 'Esteban', 'Guillen', 'eguillen@gmail.comm', '2018-02-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Moises', 'Ballesteros', 'mball@gmail.com', '2018-02-10');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Roberto', 'Piqueras', 'rpiqueras@gmail.com', '2018-02-18');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Maria', 'Barrilero', 'mbarrilero@gmail.com', '2018-02-28');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(3, 'Juan', 'Martin', 'jmartin@gmail.com', '2018-03-03');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(5, 'Oscar', 'Gonzalez', 'ogonza@gmail.com', '2018-03-04');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(6, 'Alberto', 'Martinez', 'amartinez@gmail.com', '2018-03-05');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES(7, 'Arturo', 'Llavina', 'allavina@gmail.com', '2018-03-06');

INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('sergio','$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq',1, 'Sergio', 'Juan','sjuan@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK',1, 'Admin', 'Admin','admin@gmail.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);

INSERT INTO productos (nombre, precio, create_at) VALUES('Punto de luz', 100, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Motor garaje', 400, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Cableado teléfono', 200, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Cableado vivienda', 1200, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Derivacion individual 16mm', 125, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Contador luz', 243, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('CAja derivación', 20, NOW());

INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura instalación vivienda calle Orense', null, 1, NOW());
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura instalación vivienda calle Soria', 'Don Paco', 2, NOW());

INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 6);