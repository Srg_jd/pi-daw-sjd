package com.bolsadeideas.springboot.backend.apirest.auth;

public class JwtConfig {
	public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";
	
	public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEpAIBAAKCAQEAz0sRN0dKGo4GGFQK5iFoBfl4wfpqS9tm1b7oONuXK/XaKzOL\r\n" + 
			"gNsm6Isyd0MaAYGDsnKRulvQ5xyKGawuU0F91jnPlHmgmhc+AgBdAYhw8NnfEIO8\r\n" + 
			"FHsBQ0oKn2Rs/tiGIodrNu2jfpLZaTEcZrHLxsLC1HK+GV5NpR2EOOwszXy0J4ua\r\n" + 
			"mmOrR8PVF4g7wq+wxdZ8IhxOgHDeOVypa5EosZiDElS1qdxyowtTXlf5RJCLNkSk\r\n" + 
			"GAxRuYb9gup3DhaD5YLRP0chZR8h5j1k3WJKrXtviG+UaqERx//5rbiqDojmx+KC\r\n" + 
			"SILVP18HKXI3P6SzsRXGelLJNvqOrmK7ISOTjQIDAQABAoIBAC+QrAyhIRHi0Oc4\r\n" + 
			"MaAIvlPQoLLyAP4TBA+71JFnYdLGgR80nqgcoVruxc+z5/28eDm0LFg0xUSiudWW\r\n" + 
			"OrMD7hn//e+10PJKrwHKj4zQgAnkFFk3wSp55xpHnRRh+NipWm4F4MYj1UYv8A9B\r\n" + 
			"I6E03utmuUoCYjhz+SEQGszs9ILZ1q0ivXFJFX6iVeJJWcRClXMFykuLGJ5ykL1c\r\n" + 
			"wnIfSUqUUx0ZVMKHKSx1iS7kHkgwqVnrlU5j21dxe0caQXMNC1R/DOgiWa/l59/t\r\n" + 
			"XGNliOduzuVTv+xZZCUhlhF+GXb3ecbApFXkn8zZjMXbv17zQfwXcU9mubks2ppW\r\n" + 
			"mI9giAkCgYEA8cbY9Boa2H+42Ew6ENn3BsSnCw4dr+5w+4zYy5YJXnmmm4jmWH0R\r\n" + 
			"1pk+/FXpYzSOj7ZYrb/PNadys6CEPFUBFTCLZRarj/kmq+s3j1wUzxGBarRi2Ap3\r\n" + 
			"KJCV9xknQyIf8NPcNRnyww7yRrSLqoIN6gBH08dJgnSCG9Q6CCnRhFcCgYEA23zm\r\n" + 
			"Fe/QUUVoO84CEky2Lv/NZdHisrMs4B8gSDdaLUOHCS9nLrP+ZnRjNWvTycZZDHH7\r\n" + 
			"BjYJH9PlMHD2YBWdgxviZD1SKmQc07cLNURB746ilJKiYrGmaarN4kBVKA3uLKEC\r\n" + 
			"vfiDg01xqTZqUzkimnHCUGqmywjElOm7bTz2WLsCgYAo6L1y6CvcsBfFuXglu8u5\r\n" + 
			"gHLHQxcw2oaRea6BHBzvuGPBc3OOS3NGPV3T8nFwEMeCd5cfpxHXgUK+NA3L65Io\r\n" + 
			"VLNxYlmbHcw2ULFw92Kp+stME8OZzIOHAJfvXHkxOcfvxtk+vuo9qPzDi+8J5cGW\r\n" + 
			"838zzUeZ5D7TmKBNG05RNwKBgQCf9wNp8YIN+VviWl5AIBCXaG+2SaXDP7rxDT5l\r\n" + 
			"45Ghc5mzJA07iItc+G5lvNR6NkkoPNoKyM8Vl0VubKXjmV1W9Lem/toncxdfZgSR\r\n" + 
			"KdI0Wew8BDK9Y2Q5lva+csydiKmRdTeeZMTUqVixTqtnRDjn/wjzuwcc6qL7NO4Y\r\n" + 
			"y1lviQKBgQCKYu8h+nFPizBue8q4a/my3BmoKORQWJ5NNkdIT89g0AEt1GM6VrSH\r\n" + 
			"XWy4hQ7S52e+l9qugKx5AYVJzDITmJdlpfp7Cs3ccpiwPEgHkw+mQC89wBxiUPot\r\n" + 
			"3pOOppCIYkvZjgR5KVnNSPnBwlXJe4rPyqtM7mKRBThaqnEpDcJssA==\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz0sRN0dKGo4GGFQK5iFo\r\n" + 
			"Bfl4wfpqS9tm1b7oONuXK/XaKzOLgNsm6Isyd0MaAYGDsnKRulvQ5xyKGawuU0F9\r\n" + 
			"1jnPlHmgmhc+AgBdAYhw8NnfEIO8FHsBQ0oKn2Rs/tiGIodrNu2jfpLZaTEcZrHL\r\n" + 
			"xsLC1HK+GV5NpR2EOOwszXy0J4uammOrR8PVF4g7wq+wxdZ8IhxOgHDeOVypa5Eo\r\n" + 
			"sZiDElS1qdxyowtTXlf5RJCLNkSkGAxRuYb9gup3DhaD5YLRP0chZR8h5j1k3WJK\r\n" + 
			"rXtviG+UaqERx//5rbiqDojmx+KCSILVP18HKXI3P6SzsRXGelLJNvqOrmK7ISOT\r\n" + 
			"jQIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";

}
